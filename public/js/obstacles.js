function Floor (){

    this.geometry    = new THREE.PlaneGeometry( window.innerWidth, 20 );
    this.material    = new THREE.MeshBasicMaterial( {color: 0x0B5AD9, side: THREE.DoubleSide} );
    this.plane       = new THREE.Mesh( this.geometry, this.material );
    this.plane.position.add(new THREE.Vector3(0,-window.innerHeight/2  + 10,0));
    scene.add( this.plane );
}