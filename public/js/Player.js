function player (){
    this.height      = 50;
    this.width       = 50;
    this.vel3        = new THREE.Vector3(0,0,0);
    this.aceleration = new THREE.Vector3(0,0,0);
    this.geometry    = new THREE.PlaneGeometry( 50, 50 );
    this.material    = new THREE.MeshBasicMaterial( {color: 0x00A6FF, side: THREE.DoubleSide} );
    this.plane       = new THREE.Mesh( this.geometry, this.material );
    scene.add( this.plane );

    this.move = function (map){
        let vel = 4;
        let jump = 3;
        if (( map[37] == true) && (map[32] == true)){
            // Left key
            this.plane.position.x -=  vel;
            // Jump
            this.vel3.y +=  jump;
            
        }else if (map[37] == true){
            // Left key
            this.plane.position.x -=  vel;
        }else if (map[32] == true){
            // Jump
            this.vel3.y +=  jump;
        }
        if (( map[39] == true) && (map[32] == true)){
            // Right key
            this.plane.position.x +=  vel;
            // Jump
            this.vel3.y +=  jump;
        }else if (map[39] == true){
            // Right key
            this.plane.position.x +=  vel;
        }else if (map[32] == true){
            // Jump
            this.vel3.y +=  jump;
        }
        
        
    }
    this.dynamics = function (){       
        this.plane.position.add(this.vel3)
        this.vel3.add(this.aceleration)
        this.gravity()
        this.contorno()
    }

    
    this.gravity = function (){
        this.vel3.add(new THREE.Vector3(0,-0.1,0))
    }
    this.contorno = function(){
        if (this.plane.position.x < -1*window.innerWidth*0.85/2 ){
            this.vel3.x *= 0;
        }
        if (this.plane.position.x > window.innerWidth*0.85/2 ){
            this.vel3.x *= 0;
        }
        
        if (this.plane.position.y < -1*window.innerHeight*0.85/2 ){
            this.vel3.y *= 0;
        }
    }

}