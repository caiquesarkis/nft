const scene = new THREE.Scene();
scene.background = new THREE.Color( 0x0844A3 );
const camera = new THREE.OrthographicCamera( window.innerWidth/ - 2,window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, 1, 1000 );
scene.add( camera );



const renderer = new THREE.WebGLRenderer({antialias:true,alpha:true});
renderer.setSize( window.innerWidth*0.99, window.innerHeight*0.85 );
document.body.appendChild( renderer.domElement );



// Light config
let light = new THREE.PointLight(0xFFFFFF,1,500);
light.position.set(10,0,25);

const ambient = new THREE.AmbientLight(0x404040,5);

scene.add(ambient);



let box = new player()
let floor = new Floor()
let map = [];
document.addEventListener('keydown', function(event) {       
        map[event.keyCode] = event.type == 'keydown';
        console.log(map)
        box.move(map)

});
document.addEventListener('keyup', function(event) {       
        map[event.keyCode] = event.type == 'keydown';
        console.log(map)
        box.move(map)

});

function gravity(object){
    object.vel3.add(new THREE.Vector3(0,-0.1,0))
}



camera.position.z = 10;
function animate(){
    requestAnimationFrame(animate)
    
    box.dynamics()
   
    renderer.render(scene,camera);
}

animate()